#!/bin/bash

CONFIG_LOCATION="/etc/tablet-switcher"
DEVICES_FILE="$CONFIG_LOCATION/xinput-device-ids.txt"
TABLET_COMMANDS_FILE="$CONFIG_LOCATION/commands-tablet-mode.txt"
NORMAL_COMMANDS_FILE="$CONFIG_LOCATION/commands-normal-mode.txt"

INPUT=""

function sanitize {
    INPUT=${INPUT//[^a-zA-Z0-9]/}
}

function sanitize-id {
    INPUT=${INPUT//[^0-9]/}
}


if [ "$EUID" -ne 0 ]; then
    echo "Please run as root"
    exit
fi

mkdir -p "$CONFIG_LOCATION"
rm -f "$DEVICES_FILE"
touch "$DEVICES_FILE"
touch "$NORMAL_COMMANDS_FILE"
touch "$TABLET_COMMANDS_FILE"

echo "tablet-switcher-setup"
echo
xinput
echo "--- Please select device ids to disable/enable when switching modes from above"
echo "(If youre not sure, open another terminal and type 'xinput test <device id>' for each device, and test if theres any output while using it)"
while true
do
    echo -n "enter device id ('q' to stop)> "
    read -r INPUT
    if [ "$INPUT" = "q" ]; then
        break
    fi
    sanitize-id
    echo $INPUT >> "$DEVICES_FILE"
done

echo
echo "Config files created"
echo "If you want add additional commands to be run at every mode change, check the config directory ($CONFIG_LOCATION)"
