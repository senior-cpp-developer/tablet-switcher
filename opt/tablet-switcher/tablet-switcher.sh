#!/bin/bash

CONFIG_LOCATION="/etc/tablet-switcher"
DEVICES_FILE="$CONFIG_LOCATION/xinput-device-ids.txt"
TABLET_COMMANDS_FILE="$CONFIG_LOCATION/commands-tablet-mode.txt"
NORMAL_COMMANDS_FILE="$CONFIG_LOCATION/commands-normal-mode.txt"

STATE_FILE="$XDG_RUNTIME_DIR/tablet-switcher-state"
MODE_NORMAL="normal"
MODE_TABLET="tablet"

INPUT=""

function sanitize {
    INPUT=${INPUT//[^a-zA-Z0-9]/}
}

function sanitize-id {
    INPUT=${INPUT//[^0-9]/}
}

function create-state-file {
    echo $MODE_NORMAL > "$STATE_FILE"
}

function notify {
    notify-send --expire-time="$4" --icon="$3" "tablet-switcher: $1" "$2"
    echo "tablet-switcher: $1"
    echo "$2"
}

function change-state {
    echo "---Changing state to: $1"

    while read -r INPUT; do
        sanitize-id
        echo "Device id: $INPUT"
        xinput $1 $INPUT
    done < "$DEVICES_FILE"
}

function run-commands {
    while read -r INPUT; do
        ($INPUT &) &>/dev/null
    done < "$1"
}



if [ ! -d $CONFIG_LOCATION ] || [ ! -f $DEVICES_FILE ] || [ ! -f $TABLET_COMMANDS_FILE ] || [ ! -f $NORMAL_COMMANDS_FILE ]; then
    notify "Config not found" "Please run 'sudo /opt/tablet-switcher/tablet-switcher-setup.sh' to setup config files" emblem-error 15000
    exit
fi

if [ ! -f "$STATE_FILE" ]; then
    create-state-file
fi
read -r INPUT < "$STATE_FILE"
sanitize

if [ "$INPUT" = "$MODE_NORMAL" ]; then
    notify "Tablet mode" "Turning off devices and changing to tablet mode" tablet 3000
    change-state disable
    run-commands "$TABLET_COMMANDS_FILE"
    echo $MODE_TABLET > "$STATE_FILE"
    exit
fi

if [ "$INPUT" = "$MODE_TABLET" ]; then
    notify "Normal mode" "Turning on devices and changing to normal mode" computer-laptop 3000
    change-state enable
    run-commands "$NORMAL_COMMANDS_FILE"
    echo $MODE_NORMAL > "$STATE_FILE"
    exit
fi

create-state-file
