# Tablet Switcher
Script for disabling devices and running commands when transitioning between tablet and normal mode in convertible  laptops

## Install the .deb package
1. Download last release [tablet-switcher.deb](/uploads/c6c9e9969a8bfc56175c45a47b272b73/tablet-switcher.deb)
2. Install it
```bash
sudo apt install ./tablet-switcher.deb
```

## Usage
1. Setup config (select which devices to disable/enable)
```bash
sudo /opt/tablet-switcher/tablet-switcher-setup.sh
```
Optionally add additional commands to execute in `/etc/tablet-switcher`

2. Use it
- Use desktop launcher (Tablet Switcher: Switch mode) (Maybe add desktop shortcut for convenience) or
- Use command line
```bash
bash /opt/tablet-switcher/tablet-switcher.sh
```

## Build the .deb package yourself  (Ubuntu)
1. Clone the repo
```bash
git clone https://gitlab.com/senior-cpp-developer/tablet-switcher.git
```
2. Set the permissions
```bash
chmod 755 tablet-switcher/opt/tablet-switcher/*
```
3. Delete README
```bash
rm tablet-switcher/README.md
```

4. Build the package
```bash
dpkg-deb --build --root-owner-group tablet-switcher
```
